using System.Text.Json.Serialization;

namespace Blueprint.Data;

/// <summary>
/// Describes a data structure.
/// </summary>
public class Schematic
{
    /// <summary>
    /// The unique id.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// The name.
    /// </summary>
    public string Name { get; set; } = default!;

    /// <summary>
    /// The description.
    /// </summary>
    public string Description { get; set; } = default!;

    /// <summary>
    /// The SQL table name.
    /// </summary>
    [JsonIgnore]
    public string TableName { get; set; } = default!;

    /// <summary>
    /// The properties.
    /// </summary>
    public ICollection<Property> Properties { get; set; } = new List<Property>();
}
