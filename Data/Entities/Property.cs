using System.Text.Json.Serialization;

namespace Blueprint.Data;

/// <summary>
/// Describes a data attribute.
/// </summary>
public class Property
{
    /// <summary>
    /// The unique id.
    /// </summary>
    public Guid Id { get; set; } 

    /// <summary>
    /// The schematic id.
    /// </summary>
    [JsonIgnore]
    public Guid SchematicId { get; set; }

    /// <summary>
    /// The schematic.
    /// </summary>
    [JsonIgnore]
    public Schematic Schematic { get; set; } = default!;

    /// <summary>
    /// The name.
    /// </summary>
    public string Name { get; set; } = default!;

    /// <summary>
    /// The description.
    /// </summary>
    public string Description { get; set; } = default!;

    /// <summary>
    /// The SQL column name.
    /// </summary>
    [JsonIgnore]
    public string ColumnName { get; set; } = default!;

    /// <summary>
    /// The view order relative to sibling properties.
    /// </summary>
    [JsonIgnore]
    public int ViewOrder { get; set; }

    /// <summary>
    /// The property type id.
    /// </summary>
    [JsonIgnore]
    public Guid PropertyTypeId { get; set; }

    /// <summary>
    /// The property type.
    /// </summary>
    public PropertyType PropertyType { get; set; } = default!;
}
