namespace Blueprint.Data;

/// <summary>
/// Describes the type of a property.
/// </summary>
public class PropertyType
{
    /// <summary>
    /// The unique id.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// A unique code for human-readable purposes.
    /// </summary>
    public string Code { get; set; } = default!;

    /// <summary>
    /// The name.
    /// </summary>
    public string Name { get; set; } = default!;

    /// <summary>
    /// The description.
    /// </summary>
    public string Description { get; set; } = default!;
}
