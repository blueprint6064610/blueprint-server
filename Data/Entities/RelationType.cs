namespace Blueprint.Data;

public enum RelationType
{
    OneToOne,
    OneToMany,
    ManyToOne,
    ManyToMany
}
