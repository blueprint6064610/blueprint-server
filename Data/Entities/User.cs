using System.Text.Json.Serialization;

namespace Blueprint.Data;

/// <summary>
/// A system user.
/// </summary>
public class User
{
    /// <summary>
    /// The user unique id.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// The users first name.
    /// </summary>
    public string FirstName { get; set; } = string.Empty;

    /// <summary>
    /// The users last name.
    /// </summary>
    public string LastName { get; set; } = string.Empty;

    /// <summary>
    /// The users email address.
    /// </summary>
    public string Email { get; set; } = string.Empty;

    /// <summary>
    /// The users password hash.
    /// </summary>
    [JsonIgnore]
    public byte[] PasswordHash { get; set; } = new byte[0];

    /// <summary>
    /// The users password salt.
    /// </summary>
    [JsonIgnore]
    public byte[] PasswordSalt { get; set; } = new byte[0];
}
