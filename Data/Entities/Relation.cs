namespace Blueprint.Data;

public class Relation
{
    public Guid Id { get; set; }

    public string Name { get; set; } = default!;

    public string Description { get; set; } = default!;

    public string JoinTableName { get; set; } = default!;

    public Guid LhsSchematicId { get; set; }

    public Schematic LhsSchematic { get; set; } = default!;

    public Guid RhsSchematicId { get; set; }

    public Schematic RhsSchematic { get; set; } = default!;

    public RelationType RelationType { get; set; } = default!;
}
