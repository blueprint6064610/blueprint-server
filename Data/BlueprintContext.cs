using Npgsql;
using System.Data;

namespace Blueprint.Data;

/// <summary>
/// The database context.
/// </summary>
public class BlueprintContext
{
    /// <summary>
    /// The configuration.
    /// </summary>
    private readonly IConfiguration configuration;

    /// <summary>
    /// Get the SQL connection string.
    /// </summary>
    private string SqlConnectionString => configuration.GetConnectionString("SqlConnection");

    /// <summary>
    /// BlueprintContext constructor.
    /// </summary>
    public BlueprintContext(IConfiguration configuration)
    {
        this.configuration = configuration;
    }

    /// <summary>
    /// Create a new SQL connection.
    /// </summary>
    /// <returns>The connection.</returns>
    public IDbConnection CreateConnection()
    {
        return new NpgsqlConnection(SqlConnectionString);
    }
}
