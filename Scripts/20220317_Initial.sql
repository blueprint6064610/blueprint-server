-- Schema blueprint
CREATE SCHEMA IF NOT EXISTS blueprint;
CREATE SCHEMA IF NOT EXISTS custom;

-- Extension uuid-ossp
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Table user
CREATE TABLE blueprint.user
(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password_salt BYTEA NOT NULL,
    password_hash BYTEA NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (email)
);

INSERT INTO blueprint.user (id, first_name, last_name, email, password_salt, password_hash) VALUES
    (DEFAULT, 'admin', '', 'admin@admin.com', decode('47tYO6Uyb3oBQ7yZ7tqFzg==', 'base64'), decode('EIJP/Uct/1gFnk8zNCblvTyof7jK1L1aMrZ4TcGOB68=', 'base64'));

-- Table property_type
CREATE TABLE blueprint.property_type
(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    code VARCHAR(20) NOT NULL,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (code)
);

INSERT INTO blueprint.property_type (id, code, name, description) VALUES
    (DEFAULT, 'text', 'Text', 'A small text field.'),
    (DEFAULT, 'textarea', 'Text Area', 'A large text field.'),
    (DEFAULT, 'integer', 'Integer', 'An integer number field.'),
    (DEFAULT, 'decimal', 'Decimal', 'A decimal number field.');

-- Table schematic
CREATE TABLE blueprint.schematic
(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    table_name VARCHAR(255) NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (table_name)
);

-- Table property
CREATE TABLE blueprint.property
(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    schematic_id UUID NOT NULL,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    column_name VARCHAR(255) NOT NULL,
    view_order INTEGER NOT NULL,
    property_type_id UUID NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (schematic_id, column_name, view_order),
    FOREIGN KEY (schematic_id) REFERENCES blueprint.schematic (id),
    FOREIGN KEY (property_type_id) REFERENCES blueprint.property_type (id)
);

CREATE TYPE relation_type AS ENUM ('OneToOne', 'OneToMany', 'ManyToOne', 'ManyToMany');

-- Table relation
CREATE TABLE blueprint.relation
(
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    join_table_name VARCHAR(255) NOT NULL,
    lhs_schematic_id UUID NOT NULL,
    rhs_schematic_id UUID NOT NULL,
    relation_type relation_type NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (lhs_schematic_id) REFERENCES blueprint.schematic (id),
    FOREIGN KEY (rhs_schematic_id) REFERENCES blueprint.schematic (id)
);
