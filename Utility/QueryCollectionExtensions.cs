using System.Text.Json;

namespace Blueprint.Utility;

/// <summary>
/// Class QueryCollectionExtensions.
/// </summary>
public static class QueryCollectionExtensions
{
    /// <summary>
    /// Converts an IQueryCollection to a JsonElement.
    /// </summary>
    /// <param name="collection">The IQueryCollection.</param>
    /// <returns>The JsonElement.</returns>
    public static JsonElement ToJsonElement(this IQueryCollection collection)
    {
        var dictionary = collection.ToDictionary(e => e.Key, e => e.Value.FirstOrDefault());
        return JsonSerializer.SerializeToElement(dictionary);
    }
}
