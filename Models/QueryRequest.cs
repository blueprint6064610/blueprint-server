namespace Blueprint.Models;

/// <summary>
/// Queries some data source.
/// </summary>
public class QueryRequest
{
    /// <summary>
    /// The default page value.
    /// </summary>
    public const int DefaultPage = 0;

    /// <summary>
    /// The default page size value.
    /// </summary>
    public const int DefaultPageSize = 10;

    /// <summary>
    /// The page index.
    /// </summary>
    public int Page { get; set; } = DefaultPage;

    /// <summary>
    /// The page size.
    /// </summary>
    public int PageSize { get; set; } = DefaultPageSize;
}
