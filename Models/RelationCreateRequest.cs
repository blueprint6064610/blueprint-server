using Blueprint.Data;
using System.ComponentModel.DataAnnotations;

namespace Blueprint.Models;

/// <summary>
/// Class RelationCreateRequest.
/// </summary>
public class RelationCreateRequest
{
    /// <summary>
    /// The relation name.
    /// </summary>
    [Required]
    public string Name { get; set; } = default!;

    /// <summary>
    /// The relation description.
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// The relation left hand side schematic id.
    /// </summary>
    [Required]
    public Guid LhsSchematicId { get; set; }

    /// <summary>
    /// The relation right hand side schematic id.
    /// </summary>
    [Required]
    public Guid RhsSchematicId { get; set; }

    /// <summary>
    /// The relation type.
    /// </summary>
    [Required]
    public RelationType RelationType { get; set; }
}
