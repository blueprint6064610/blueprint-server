using System.ComponentModel.DataAnnotations;

namespace Blueprint.Models;

/// <summary>
/// Class PropertyUpdateRequest.
/// </summary>
public class PropertyUpdateRequest
{
    /// <summary>
    /// The property name.
    /// </summary>
    [Required]
    public string Name { get; set; } = default!;

    /// <summary>
    /// The property description.
    /// </summary>
    public string Description { get; set; } = string.Empty;
}
