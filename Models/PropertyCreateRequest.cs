using System.ComponentModel.DataAnnotations;

namespace Blueprint.Models;

/// <summary>
/// Class PropertyCreateRequest.
/// </summary>
public class PropertyCreateRequest
{
    /// <summary>
    /// The property name.
    /// </summary>
    [Required]
    public string Name { get; set; } = default!;

    /// <summary>
    /// The property description.
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// The property type id.
    /// </summary>
    [Required]
    public Guid PropertyTypeId { get; set; }
}
