using System.ComponentModel.DataAnnotations;

namespace Blueprint.Models;

/// <summary>
/// Class SchematicCreateRequest.
/// </summary>
public class SchematicCreateRequest
{
    /// <summary>
    /// The schematic name.
    /// </summary>
    [Required]
    public string Name { get; set; } = default!;

    /// <summary>
    /// The schematic description.
    /// </summary>
    public string Description { get; set; } = string.Empty;
}
