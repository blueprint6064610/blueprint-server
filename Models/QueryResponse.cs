namespace Blueprint.Models;

/// <summary>
/// A response for a query.
/// </summary>
/// <typeparam name="T">The entity.</typeparam>
public class QueryResponse<T>
{
    /// <summary>
    /// The total number of items matching the query, excluding pagination.
    /// </summary>
    public int Total { get; set; }

    /// <summary>
    /// The items matching the query within the page.
    /// </summary>
    public IEnumerable<T> Items { get; set; }

    /// <summary>
    /// QueryResponse constructor.
    /// </summary>
    /// <param name="total">The total.</param>
    /// <param name="items">The items.</param>
    public QueryResponse(int total, IEnumerable<T> items)
    {
        Total = total;
        Items = items;
    }
}
