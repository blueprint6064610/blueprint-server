namespace Blueprint.Models;

/// <summary>
/// Class UserFindRequest.
/// </summary>
public class UserFindRequest : QueryRequest
{
    /// <summary>
    /// The first name filter.
    /// </summary>
    public string? FirstName { get; set; }

    /// <summary>
    /// The last name filter.
    /// </summary>
    public string? LastName { get; set; }

    /// <summary>
    /// The email filter.
    /// </summary>
    public string? Email { get; set; }
}
