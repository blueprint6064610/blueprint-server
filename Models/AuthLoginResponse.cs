namespace Blueprint.Models;

/// <summary>
/// Class AuthLoginResponse.
/// </summary>
public class AuthLoginResponse
{
    /// <summary>
    /// The access token.
    /// </summary>
    public string Token { get; set; } = string.Empty;
}
