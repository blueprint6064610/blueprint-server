using System.ComponentModel.DataAnnotations;

namespace Blueprint.Models;

/// <summary>
/// Class UserCreateRequest.
/// </summary>
public class UserCreateRequest
{
    /// <summary>
    /// The users first name.
    /// </summary>
    [Required]
    public string FirstName { get; set; } = string.Empty;

    /// <summary>
    /// The users last name.
    /// </summary>
    [Required]
    public string LastName { get; set; } = string.Empty;

    /// <summary>
    /// The users email address.
    /// </summary>
    [EmailAddress]
    public string Email { get; set; } = string.Empty;

    /// <summary>
    /// The users password.
    /// </summary>
    [Required]
    public string Password { get; set; } = string.Empty;
}
