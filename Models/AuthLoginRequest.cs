using System.ComponentModel.DataAnnotations;

namespace Blueprint.Models;

/// <summary>
/// Class AuthLoginRequest.
/// </summary>
public class AuthLoginRequest
{
    /// <summary>
    /// The email address to login.
    /// </summary>
    [EmailAddress]
    public string Email { get; set; } = string.Empty;

    /// <summary>
    /// The password.
    /// </summary>
    [Required]
    public string Password { get; set; } = string.Empty;
}
