namespace Blueprint.Models;

/// <summary>
/// Class SchematicFindRequest.
/// </summary>
public class SchematicFindRequest : QueryRequest
{
    /// <summary>
    /// The name filter.
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// The description filter.
    /// </summary>
    public string? Description { get; set; }
}
