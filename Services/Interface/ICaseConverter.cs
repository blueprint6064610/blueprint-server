namespace Blueprint.Services;

/// <summary>
/// Converts from one case to another.
/// </summary>
public interface ICaseConverter
{
    /// <summary>
    /// Convert text into the target case.
    /// </summary>
    /// <param name="text">The text.</param>
    /// <returns>The text in the target case.</returns>
    string Convert(string text);
}
