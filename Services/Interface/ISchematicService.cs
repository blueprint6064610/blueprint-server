using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Services;

/// <summary>
/// Interface ISchematicService.
/// </summary>
public interface ISchematicService
{
    /// <summary>
    /// Create a schematic.
    /// </summary>
    /// <param name="request">The create schematic request.</param>
    /// <returns>The schematic.</returns>
    Task<bool> Create(SchematicCreateRequest request);

    /// <summary>
    /// Delete a schematic.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>Success or failure.</returns>
    Task<bool> Delete(Guid id);

    /// <summary>
    /// Find schematics that match the query.
    /// </summary>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<Schematic>> Find(SchematicFindRequest request);

    /// <summary>
    /// Find a schematic by id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>The schematic.</returns>
    Task<Schematic?> FindById(Guid id);

    /// <summary>
    /// Update a schematic.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <param name="request">The update schematic request.</param>
    /// <returns>If successful.</returns>
    Task<bool> Update(Guid id, SchematicUpdateRequest request);
}
