using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Services;

/// <summary>
/// Interface IUserService.
/// </summary>
public interface IUserService
{
    /// <summary>
    /// Creates a user.
    /// </summary>
    /// <param name="request">The create user request.</param>
    /// <returns>The user.</returns>
    Task<bool> Create(UserCreateRequest request);

    /// <summary>
    /// Deletes a user by id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>Task</returns>
    Task<bool> Delete(Guid id);

    /// <summary>
    /// Finds all users.
    /// </summary>
    /// <returns>The users.</returns>
    Task<QueryResponse<User>> Find(UserFindRequest request);

    /// <summary>
    /// Finds a user by the email address.
    /// </summary>
    /// <param name="email">The email address.</param>
    /// <returns>The user if found, otherwise null.</returns>
    Task<User?> FindByEmail(string email);

    /// <summary>
    /// Finds a user by the unique id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>The user if found, otherwise null.</returns>
    Task<User?> FindById(Guid id);

    /// <summary>
    /// Updates a user.
    /// </summary>
    /// <param name="id">The id of the user.</param>
    /// <param name="request">The update user request.</param>
    /// <returns>The user.</returns>
    Task<bool> Update(Guid id, UserUpdateRequest request);
}
