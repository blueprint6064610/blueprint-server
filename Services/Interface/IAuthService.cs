using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Services;

/// <summary>
/// Interface IAuthService.
/// </summary>
public interface IAuthService
{
    /// <summary>
    /// Validates the users credentials.
    /// </summary>
    /// <param name="request">The login request.</param>
    /// <returns>If the user is validated.</returns>
    Task<bool> ValidateUser(AuthLoginRequest request);

    /// <summary>
    /// Generates an authentication token.
    /// </summary>
    /// <param name="user">The user to generate the token for.</param>
    /// <returns>The authentication token.</returns>
    Task<string> GenerateToken(User user);
}