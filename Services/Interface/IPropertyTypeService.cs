using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Services;

/// <summary>
/// Interface IPropertyTypeService.
/// </summary>
public interface IPropertyTypeService
{
    /// <summary>
    /// Find property types that match the query.
    /// </summary>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<PropertyType>> Find(QueryRequest request);

    /// <summary>
    /// Find a property type by id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>The property type.</returns>
    Task<PropertyType?> FindById(Guid id);
}
