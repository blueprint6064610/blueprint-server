namespace Blueprint.Services;

/// <summary>
/// Password hashing operations.
/// </summary>
public interface IPasswordHasher
{
    /// <summary>
    /// Generates a password hash.
    /// </summary>
    /// <param name="digest">The input digest.</param>
    /// <param name="salt">The output salt.</param>
    /// <param name="hash">The output hash.</param>
    /// <returns>Task</returns>
    Task HashPassword(string digest, out byte[] salt, out byte[] hash);

    /// <summary>
    /// Verifies a string against the hashed password.
    /// </summary>
    /// <param name="attempt">The password attempt.</param>
    /// <param name="salt">The target salt.</param>
    /// <param name="hash">The target hash.</param>
    /// <returns></returns>
    Task<bool> VerifyHash(string attempt, byte[] salt, byte[] hash);
}