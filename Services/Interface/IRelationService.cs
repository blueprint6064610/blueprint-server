using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Services;

/// <summary>
/// Interface IRelationService.
/// </summary>
public interface IRelationService
{
    /// <summary>
    /// Create a relation.
    /// </summary>
    /// <param name="request">The relation create request.</param>
    /// <returns>If succesful.</returns>
    Task<bool> Create(RelationCreateRequest request);

    /// <summary>
    /// Get relations for a schematic.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<Relation>> FindBySchematic(Guid id, QueryRequest request);

    /// <summary>
    /// Delete a relation.
    /// </summary>
    /// <param name="id">The relation id.</param>
    /// <returns>If successful.</returns>
    Task<bool> Delete(Guid id);
}
