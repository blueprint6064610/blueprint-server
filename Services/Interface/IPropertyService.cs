using Blueprint.Models;
using Blueprint.Data;

namespace Blueprint.Services;

public interface IPropertyService
{
    Task<bool> Create(Guid schematicId, PropertyCreateRequest request);

    Task<bool> Delete(Guid schematicId, Guid id);

    Task<QueryResponse<Property>> FindBySchematic(Guid schematicId, QueryRequest request);

    Task<bool> Update(Guid schematicId, Guid id, PropertyUpdateRequest request);
}
