using Blueprint.Models;
using System.Text.Json;

namespace Blueprint.Services;

public interface ICustomService
{
    /// <summary>
    /// Create a custom entity.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="request">The create entity request.</param>
    /// <returns>If successful.</returns>
    Task<bool> Create(Guid schematicId, JsonElement request);

    /// <summary>
    /// Delete a custom entity.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="id">The custom entity id.</param>
    /// <returns>If successful.</returns>
    Task<bool> Delete(Guid schematicId, Guid id);

    /// <summary>
    /// Find custom entities based on a query.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<object>> Find(Guid schematicId, JsonElement request);

    /// <summary>
    /// Find a custom entity by id.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="id">The custom entity id.</param>
    /// <returns>The custom entity if found.</returns>
    Task<object?> FindById(Guid schematicId, Guid id);

    /// <summary>
    /// Update a custom entity.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="id">The entity id.</param>
    /// <param name="request">The update request.</param>
    /// <returns>If successful.</returns>
    Task<bool> Update(Guid schematicId, Guid id, JsonElement request);
}
