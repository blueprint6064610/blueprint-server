using Blueprint.Data;

namespace Blueprint.Services;

/// <summary>
/// Interface ISqlTypeResolver.
/// </summary>
public interface ISqlTypeResolver
{
    /// <summary>
    /// Resolves a property type to an SQL type.
    /// </summary>
    /// <param name="propertyType">The property type.</param>
    /// <returns>The SQL type.</returns>
    string Resolve(PropertyType propertyType);
}
