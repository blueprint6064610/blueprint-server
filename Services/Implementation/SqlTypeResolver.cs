using Blueprint.Data;

namespace Blueprint.Services;

/// <summary>
/// Class SqlTypeResolver.
/// </summary>
public class SqlTypeResolver : ISqlTypeResolver
{
    /// <inheritdoc/>
    public string Resolve(PropertyType propertyType)
    {
        switch (propertyType.Code)
        {
            case "textarea":
                return "TEXT";
            case "text":
                return "VARCHAR(255)";
            case "integer":
                return "INT";
            case "decimal":
                return "DECIMAL";
            default:
                throw new Exception("Unknown property type");
        }
    }
}
