using Blueprint.Data;
using Blueprint.Models;
using Blueprint.Repositories;

namespace Blueprint.Services;

/// <summary>
/// Class RelationService.
/// </summary>
public class RelationService : IRelationService
{
    /// <summary>
    /// The relation repository.
    /// </summary>
    private readonly IRelationRepository relationRepository;

    /// <summary>
    /// The schematic repository.
    /// </summary>
    private readonly ISchematicRepository schematicRepository;

    /// <summary>
    /// The snake case converter.
    /// </summary>
    private readonly ISnakeCaseConverter snakeCaseConverter;

    /// <summary>
    /// RelationService constructor.
    /// </summary>
    public RelationService(IRelationRepository relationRepository, ISchematicRepository schematicRepository, ISnakeCaseConverter snakeCaseConverter)
    {
        this.relationRepository = relationRepository;
        this.schematicRepository = schematicRepository;
        this.snakeCaseConverter = snakeCaseConverter;
    }

    /// <inheritdoc/>
    public async Task<bool> Create(RelationCreateRequest request)
    {
        var relation = new Relation
        {
            Name = request.Name,
            Description = request.Description,
            JoinTableName = GetIdentifier(request.Name),
            LhsSchematicId = request.LhsSchematicId,
            LhsSchematic = await GetSchematic(request.LhsSchematicId),
            RhsSchematicId = request.RhsSchematicId,
            RhsSchematic = await GetSchematic(request.RhsSchematicId),
            RelationType = request.RelationType
        };

        await relationRepository.Create(relation);
        return true;
    }

    /// <inheritdoc/>
    public async Task<QueryResponse<Relation>> FindBySchematic(Guid id, QueryRequest request)
    {
        return await relationRepository.FindBySchematic(id, request);
    }

    /// <inheritdoc/>
    public async Task<bool> Delete(Guid id)
    {
        var relation = await relationRepository.FindById(id);
        if (relation == null) return false;

        await relationRepository.Delete(relation);
        return true;
    }

    /// <summary>
    /// Get the identifier.
    /// </summary>
    /// <param name="name">The input name.</param>
    /// <returns>The identifier.</returns>
    private string GetIdentifier(string name)
    {
        var identifier = snakeCaseConverter.Convert(name);
        if (string.IsNullOrEmpty(identifier))
        {
            throw new Exception("Identifier is invalid");
        }

        return identifier;
    }

    /// <summary>
    /// Get the schematic.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private async Task<Schematic> GetSchematic(Guid id)
    {
        var schematic = await schematicRepository.FindById(id);
        if (schematic == null)
        {
            throw new Exception($"Schematic not found: {id}");
        }

        return schematic;
    }
}
