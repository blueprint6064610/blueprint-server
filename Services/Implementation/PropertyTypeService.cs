using Blueprint.Data;
using Blueprint.Models;
using Blueprint.Repositories;

namespace Blueprint.Services;

/// <summary>
/// Class PropertyTypeService.
/// </summary>
public class PropertyTypeService : IPropertyTypeService
{
    /// <summary>
    /// The property type repository.
    /// </summary>
    private readonly IPropertyTypeRepository propertyTypeRepository;

    /// <summary>
    /// PropertyTypeService constructor.
    /// </summary>
    public PropertyTypeService(IPropertyTypeRepository propertyTypeRepository)
    {
        this.propertyTypeRepository = propertyTypeRepository;
    }

    /// <inheritdoc/>
    public Task<QueryResponse<PropertyType>> Find(QueryRequest request)
    {
        return propertyTypeRepository.Find(request);
    }

    /// <inheritdoc/>
    public Task<PropertyType?> FindById(Guid id)
    {
        return propertyTypeRepository.FindById(id);
    }
}
