using Blueprint.Data;
using Blueprint.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Blueprint.Services;

/// <summary>
/// Class AuthService.
/// </summary>
public class AuthService : IAuthService
{
    /// <summary>
    /// The configuration.
    /// </summary>
    private readonly IConfiguration configuration;

    /// <summary>
    /// The password hasher.
    /// </summary>
    private readonly IPasswordHasher passwordHasher;

    /// <summary>
    /// The user service.
    /// </summary>
    private readonly IUserService userService;

    /// <summary>
    /// AuthService constructor.
    /// </summary>
    public AuthService(IConfiguration configuration, IPasswordHasher passwordHasher, IUserService userService)
    {
        this.configuration = configuration;
        this.passwordHasher = passwordHasher;
        this.userService = userService;
    }

    /// <inheritdoc/>
    public async Task<bool> ValidateUser(AuthLoginRequest request)
    {
        var user = await userService.FindByEmail(request.Email);
        if (user == null)
        {
            return false;
        }

        return await passwordHasher.VerifyHash(request.Password, user.PasswordSalt, user.PasswordHash);
    }

    /// <inheritdoc/>
    public Task<string> GenerateToken(User user)
    {
        var claims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName),
            new Claim(ClaimTypes.Email, user.Email),
            new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(DateTime.Now.AddDays(1)).ToUnixTimeSeconds().ToString())
        };

        var key = configuration.GetSection("Jwt")["IssuerKey"];
        var header = new JwtHeader(
            new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
                SecurityAlgorithms.HmacSha256
            )
        );

        var payload = new JwtPayload(claims);
        var token = new JwtSecurityToken(header, payload);
        var jwt = new JwtSecurityTokenHandler().WriteToken(token);
        return Task.FromResult(jwt);
    }
}
