using Blueprint.Data;
using Blueprint.Models;
using Blueprint.Repositories;

namespace Blueprint.Services;

/// <summary>
/// Implementation of IUserService.
/// </summary>
public class UserService : IUserService
{
    /// <summary>
    /// The password hasher.
    /// </summary>
    private readonly IPasswordHasher passwordHasher;

    /// <summary>
    /// The logger.
    /// </summary>
    private readonly ILogger<UserService> logger;

    /// <summary>
    /// The user repository.
    /// </summary>
    private readonly IUserRepository userRepository;

    /// <summary>
    /// UserService constructor.
    /// </summary>
    public UserService(IPasswordHasher passwordHasher, ILogger<UserService> logger, IUserRepository userRepository)
    {
        this.passwordHasher = passwordHasher;
        this.logger = logger;
        this.userRepository = userRepository;
    }

    /// <inheritdoc/>
    public async Task<bool> Create(UserCreateRequest request)
    {
        byte[] salt;
        byte[] hash;
        await passwordHasher.HashPassword(request.Password, out salt, out hash);

        var user = new User
        {
            FirstName = request.FirstName,
            LastName = request.LastName,
            Email = request.Email,
            PasswordSalt = salt,
            PasswordHash = hash
        };

        await userRepository.Create(user);
        return true;
    }

    /// <inheritdoc/>
    public async Task<bool> Delete(Guid id)
    {
        await userRepository.Delete(id);
        return true;
    }

    /// <inheritdoc/>
    public Task<QueryResponse<User>> Find(UserFindRequest request)
    {
        return userRepository.Find(request);
    }

    /// <inheritdoc/>
    public Task<User?> FindByEmail(string email)
    {
        return userRepository.FindByEmail(email);
    }

    /// <inheritdoc/>
    public Task<User?> FindById(Guid id)
    {
        return userRepository.FindById(id);
    }

    /// <inheritdoc/>
    public async Task<bool> Update(Guid id, UserUpdateRequest request)
    {
        byte[] salt;
        byte[] hash;
        await passwordHasher.HashPassword(request.Password, out salt, out hash);

        var user = new User
        {
            Id = id,
            FirstName = request.FirstName,
            LastName = request.LastName,
            Email = request.Email,
            PasswordSalt = salt,
            PasswordHash = hash
        };

        await userRepository.Update(user);
        return true;
    }
}
