using Blueprint.Data;
using Blueprint.Models;
using Blueprint.Repositories;

namespace Blueprint.Services;

public class PropertyService : IPropertyService
{
    private readonly IPropertyRepository propertyRepository;

    private readonly IPropertyTypeRepository propertyTypeRepository;

    private readonly ISchematicRepository schematicRepository;

    private readonly ISnakeCaseConverter snakeCaseConverter;

    public PropertyService(
        IPropertyRepository propertyRepository,
        IPropertyTypeRepository propertyTypeRepository,
        ISchematicRepository schematicRepository,
        ISnakeCaseConverter snakeCaseConverter)
    {
        this.propertyRepository = propertyRepository;
        this.propertyTypeRepository = propertyTypeRepository;
        this.schematicRepository = schematicRepository;
        this.snakeCaseConverter = snakeCaseConverter;
    }

    public async Task<bool> Create(Guid schematicId, PropertyCreateRequest request)
    {
        var schematic = await GetSchematic(schematicId);
        var propertyType = await GetPropertyType(request.PropertyTypeId);

        var property = new Property
        {
            SchematicId = schematicId,
            Schematic = schematic,
            Name = request.Name,
            Description = request.Description,
            ColumnName = GetIdentifier(request.Name),
            ViewOrder = schematic.Properties.Count + 1,
            PropertyTypeId = request.PropertyTypeId,
            PropertyType = propertyType
        };

        await propertyRepository.Create(property);
        return true;
    }

    public async Task<bool> Delete(Guid schematicId, Guid id)
    {
        var schematic = await GetSchematic(schematicId);
        var property = await propertyRepository.FindById(id);
        if (property == null) return false;

        property.Schematic = schematic;

        await propertyRepository.Delete(property);
        return true;
    }

    public async Task<QueryResponse<Property>> FindBySchematic(Guid schematicId, QueryRequest request)
    {
        return await propertyRepository.FindBySchematic(schematicId, request);
    }

    public async Task<bool> Update(Guid schematicId, Guid id, PropertyUpdateRequest request)
    {
        var schematic = await GetSchematic(schematicId);

        var property = new Property
        {
            Id = id,
            SchematicId = schematicId,
            Schematic = schematic,
            Name = request.Name,
            Description = request.Description,
            ColumnName = GetIdentifier(request.Name)
        };

        await propertyRepository.Update(property);
        return true;
    }

    private async Task<Schematic> GetSchematic(Guid id)
    {
        var schematic = await schematicRepository.FindById(id);
        if (schematic == null) throw new Exception();
        return schematic;
    }

    private async Task<PropertyType> GetPropertyType(Guid id)
    {
        var propertyType = await propertyTypeRepository.FindById(id);
        if (propertyType == null) throw new Exception();
        return propertyType;
    }

    private string GetIdentifier(string name)
    {
        var identifier = snakeCaseConverter.Convert(name);
        if (string.IsNullOrEmpty(identifier))
        {
            throw new Exception("Identifier name is invalid");
        }

        return identifier;
    }
}
