using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Blueprint.Services;

/// <summary>
/// PBKDF2 implementation of password hasher.
/// </summary>
public class Pbkdf2PasswordHasher : IPasswordHasher
{
    /// <summary>
    /// The number of bytes in the output hash.
    /// </summary>
    private const int HashBytes = 32;

    /// <summary>
    /// The number of bytes in the output salt.
    /// </summary>
    private const int SaltBytes = 16;

    /// <summary>
    /// The number of iterations to use for the hash.
    /// </summary>
    private const int Iterations = 100_000;

    /// <inheritdoc/>
    public Task HashPassword(string digest, out byte[] salt, out byte[] hash)
    {
        // Generate salt
        salt = RandomNumberGenerator.GetBytes(SaltBytes);

        // Generate hash
        hash = KeyDerivation.Pbkdf2(
            password: digest,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: Iterations,
            numBytesRequested: HashBytes
        );

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public Task<bool> VerifyHash(string attempt, byte[] salt, byte[] hash)
    {
        var attemptHash = KeyDerivation.Pbkdf2(
            password: attempt,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: Iterations,
            numBytesRequested: HashBytes
        );

        var result = Enumerable.SequenceEqual(attemptHash, hash);
        return Task.FromResult(result);
    }
}
