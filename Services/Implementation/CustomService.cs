using Blueprint.Data;
using Blueprint.Models;
using Blueprint.Repositories;
using System.Text.Json;

namespace Blueprint.Services;

/// <summary>
/// Class CustomService.
/// </summary>
public class CustomService : ICustomService
{
    /// <summary>
    /// The custom repository.
    /// </summary>
    private readonly ICustomRepository customRepository;

    /// <summary>
    /// The schematic repository.
    /// </summary>
    private readonly ISchematicRepository schematicRepository;

    /// <summary>
    /// CustomService constructor.
    /// </summary>
    public CustomService(ICustomRepository customRepository, ISchematicRepository schematicRepository)
    {
        this.customRepository = customRepository;
        this.schematicRepository = schematicRepository;
    }

    /// <inheritdoc/>
    public async Task<bool> Create(Guid schematicId, JsonElement request)
    {
        var schematic = await GetSchematic(schematicId);
        await customRepository.Create(schematic, request);
        return true;
    }

    /// <inheritdoc/>
    public async Task<bool> Delete(Guid schematicId, Guid entityId)
    {
        var schematic = await GetSchematic(schematicId);
        await customRepository.Delete(schematic, entityId);
        return true;
    }

    /// <inheritdoc/>
    public async Task<QueryResponse<object>> Find(Guid schematicId, JsonElement request)
    {
        var schematic = await GetSchematic(schematicId);
        return await customRepository.Find(schematic, request);
    }

    /// <inheritdoc/>
    public async Task<object?> FindById(Guid schematicId, Guid entityId)
    {
        var schematic = await GetSchematic(schematicId);
        return await customRepository.FindById(schematic, entityId);
    }

    /// <inheritdoc/>
    public async Task<bool> Update(Guid schematicId, Guid entityId, JsonElement request)
    {
        var schematic = await GetSchematic(schematicId);
        await customRepository.Update(schematic, entityId, request);
        return true;
    }

    /// <summary>
    /// Get the schematic from id.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <returns>The schematic.</returns>
    /// <exception cref="Exception">Throws if not found.</exception>
    private async Task<Schematic> GetSchematic(Guid id)
    {
        var schematic = await schematicRepository.FindById(id);
        if (schematic == null)
        {
            throw new Exception("Schematic not found");
        }

        return schematic;
    }
}
