using System.Text.RegularExpressions;

namespace Blueprint.Services;

/// <summary>
/// Class SnakeCaseConverter.
/// </summary>
public class SnakeCaseConverter : ISnakeCaseConverter
{
    /// <summary>
    /// Regex matches on any non-alphanumeric character (excluding space, underscore and dash).
    /// </summary>
    private static readonly Regex regex = new Regex("[^a-zA-Z0-9 _-]");

    /// <inheritdoc/>
    public string Convert(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            return text;
        }

        var snakeCase = regex.Replace(text, "")
            .Trim()
            .ToLower()
            .Replace(' ', '_')
            .Replace('-', '_');

        // Make sure identifier does not start with digit
        if (snakeCase.Length > 0 && char.IsDigit(snakeCase[0]))
        {
            snakeCase = '_' + snakeCase;
        }

        return snakeCase;
    }
}
