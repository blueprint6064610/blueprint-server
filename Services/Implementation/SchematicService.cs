using Blueprint.Data;
using Blueprint.Models;
using Blueprint.Repositories;

namespace Blueprint.Services;

/// <summary>
/// Class SchematicService.
/// </summary>
public class SchematicService : ISchematicService
{
    /// <summary>
    /// The schematic repository.
    /// </summary>
    private readonly ISchematicRepository schematicRepository;

    /// <summary>
    /// The snake case converter.
    /// </summary>
    private readonly ISnakeCaseConverter snakeCaseConverter;

    /// <summary>
    /// SchematicService constructor.
    /// </summary>
    public SchematicService(
        ISchematicRepository schematicRepository,
        ISnakeCaseConverter snakeCaseConverter)
    {
        this.schematicRepository = schematicRepository;
        this.snakeCaseConverter = snakeCaseConverter;
    }

    /// <inheritdoc/>
    public async Task<bool> Create(SchematicCreateRequest request)
    {
        var schematic = new Schematic
        {
            Name = request.Name,
            Description = request.Description,
            TableName = GetIdentifier(request.Name)
        };

        await schematicRepository.Create(schematic);
        return true;
    }

    /// <inheritdoc/> 
    public async Task<bool> Delete(Guid id)
    {
        await schematicRepository.Delete(id);
        return true;
    }

    /// <inheritdoc/> 
    public Task<QueryResponse<Schematic>> Find(SchematicFindRequest request)
    {
        return schematicRepository.Find(request);
    }

    /// <inheritdoc/> 
    public Task<Schematic?> FindById(Guid id)
    {
        return schematicRepository.FindById(id);
    }

    /// <inheritdoc/> 
    public async Task<bool> Update(Guid id, SchematicUpdateRequest request)
    {
        var schematic = new Schematic
        {
            Id = id,
            Name = request.Name,
            Description = request.Description,
            TableName = GetIdentifier(request.Name)
        };

        await schematicRepository.Update(schematic);
        return true;
    }

    private string GetIdentifier(string name)
    {
        var identifier = snakeCaseConverter.Convert(name);
        if (string.IsNullOrEmpty(identifier))
        {
            throw new Exception("Identifier name is invalid");
        }

        return identifier;
    }
}
