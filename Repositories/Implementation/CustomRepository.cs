using Blueprint.Data;
using Blueprint.Models;
using Dapper;
using System.Text;
using System.Text.Json;

namespace Blueprint.Repositories;

/// <summary>
/// Class CustomRepository.
/// </summary>
public class CustomRepository : ICustomRepository
{
    /// <summary>
    /// The blueprint context.
    /// </summary>
    private readonly BlueprintContext context;

    private readonly ILogger<CustomRepository> logger;

    /// <summary>
    /// CustomRepository constructor.
    /// </summary>
    public CustomRepository(BlueprintContext context, ILogger<CustomRepository> logger)
    {
        this.context = context;
        this.logger = logger;
    }

    /// <inheritdoc/>
    public async Task Create(Schematic schematic, JsonElement request)
    {
        var parameters = new DynamicParameters();
        var createSql = GetCreateSql(schematic, request, parameters);

        using (var connection = context.CreateConnection())
        {
            await connection.ExecuteAsync(createSql, parameters);
        }
    }

    /// <inheritdoc/>
    public async Task Delete(Schematic schematic, Guid id)
    {
        var parameters = new DynamicParameters();
        var deleteSql = GetDeleteSql(schematic, id, parameters);

        using (var connection = context.CreateConnection())
        {
            await connection.ExecuteAsync(deleteSql, parameters);
        }
    }

    /// <inheritdoc/>
    public async Task<QueryResponse<object>> Find(Schematic schematic, JsonElement request)
    {
        var parameters = new DynamicParameters();
        var whereClause = GetFindWhereSql(schematic, request, parameters);
        var findTotalSql = GetFindTotalSql(schematic, whereClause);
        var findSql = GetFindSql(schematic, request, whereClause, parameters);

        using (var connection = context.CreateConnection())
        {
            var total = await connection.QuerySingleAsync<int>(findTotalSql, parameters);
            var results = await connection.QueryAsync(findSql, parameters);
            return new QueryResponse<object>(total, results);
        }
    }

    /// <inheritdoc/>
    public async Task<object?> FindById(Schematic schematic, Guid id)
    {
        var parameters = new DynamicParameters();
        var findByIdSql = GetFindByIdSql(schematic, id, parameters);

        using (var connection = context.CreateConnection())
        {
            return await connection.QuerySingleOrDefaultAsync<object>(findByIdSql, parameters);
        }
    }

    /// <inheritdoc/>
    public async Task Update(Schematic schematic, Guid id, JsonElement request)
    {
        DynamicParameters parameters = new DynamicParameters();
        var updateSql = GetUpdateSql(schematic, id, request, parameters);

        using (var connection = context.CreateConnection())
        {
            await connection.ExecuteAsync(updateSql, parameters);
        }
    }

    private string GetCreateSql(Schematic schematic, JsonElement request, DynamicParameters parameters)
    {
        StringBuilder builder = new StringBuilder();
        builder.Append($"INSERT INTO custom.{schematic.TableName} (");
        builder.Append(string.Join(", ", schematic.Properties.Select(property => property.ColumnName)));
        builder.Append(") VALUES (");
        builder.Append(string.Join(", ", schematic.Properties.Select(property => $"@{property.ColumnName}")));

        foreach (var prop in request.EnumerateObject())
        {
            var propertyId = prop.Name;
            var property = schematic.Properties.First(property => property.Id == Guid.Parse(propertyId));
            var placeholder = $"@{property.ColumnName}";

            object? value = null;
            if (prop.Value.ValueKind == JsonValueKind.String)
            {
                value = prop.Value.GetString();
            }
            else if (prop.Value.ValueKind == JsonValueKind.Number)
            {
                value = prop.Value.GetDouble();
            }

            parameters.Add(placeholder, value);
        }

        builder.Append(")");

        return builder.ToString();
    }

    private string GetFindByIdSql(Schematic schematic, Guid id, DynamicParameters parameters)
    {
        var builder = new StringBuilder();
        builder.Append("SELECT id AS Id, ");

        int index = 0;
        foreach (var property in schematic.Properties)
        {
            builder.Append($"{property.ColumnName} AS \"{property.Id}\"");
            builder.Append((index++ < schematic.Properties.Count - 1) ? ", " : " ");
        }

        builder.Append($"FROM custom.{schematic.TableName} WHERE id = @Id");

        parameters.Add("@Id", id);

        return builder.ToString();
    }

    private string GetFindSql(Schematic schematic, JsonElement request, string whereClause, DynamicParameters parameters)
    {
        var builder = new StringBuilder();
        builder.Append("SELECT id AS Id");

        foreach (var property in schematic.Properties)
        {
            builder.Append($", {property.ColumnName} AS \"{property.Id}\"");
        }

        builder.Append($" FROM custom.{schematic.TableName} {whereClause} LIMIT @Limit OFFSET @Offset");

        int pageSize = QueryRequest.DefaultPageSize;
        int page = QueryRequest.DefaultPage;

        if (request.TryGetProperty("PageSize", out var pageSizeElement))
        {
            pageSize = pageSizeElement.GetInt32();
        }

        if (request.TryGetProperty("Page", out var pageElement))
        {
            page = pageElement.GetInt32();
        }

        parameters.Add("@Limit", pageSize);
        parameters.Add("@Offset", pageSize * page);

        return builder.ToString();
    }

    private string GetFindTotalSql(Schematic schematic, string whereClause)
    {
        return $"SELECT COUNT(*) FROM custom.{schematic.TableName} {whereClause}";
    }

    private string GetDeleteSql(Schematic schematic, Guid id, DynamicParameters parameters)
    {
        var sql = @$"DELETE FROM custom.{schematic.TableName} WHERE id = @Id";

        parameters.Add("@Id", id);

        return sql;
    }

    private string GetUpdateSql(Schematic schematic, Guid id, JsonElement request, DynamicParameters parameters)
    {
        var builder = new StringBuilder();
        builder.Append($"UPDATE custom.{schematic.TableName} SET ");

        var index = 0;
        foreach (var property in schematic.Properties)
        {
            builder.Append($"{property.ColumnName} = @{property.ColumnName}");
            builder.Append((index++ < schematic.Properties.Count - 1) ? ", " : " ");
        }

        foreach (var prop in request.EnumerateObject())
        {
            var propertyId = prop.Name;
            var property = schematic.Properties.First(property => property.Id == Guid.Parse(propertyId));
            var placeholder = $"@{property.ColumnName}";

            object? value = null;
            if (prop.Value.ValueKind == JsonValueKind.String)
            {
                value = prop.Value.GetString();
            }
            else if (prop.Value.ValueKind == JsonValueKind.Number)
            {
                value = prop.Value.GetDouble();
            }

            parameters.Add(placeholder, value);
        }

        builder.Append(" WHERE id = @Id");
        parameters.Add("@Id", id);

        return builder.ToString();
    }

    /// <summary>
    /// Build the where clause for a entity find request.
    /// </summary>
    /// <param name="request">The entity find request.</param>
    /// <param name="parameters">The SQL parameters.</param>
    /// <returns>The where clause.</returns>
    private string GetFindWhereSql(Schematic schematic, JsonElement request, DynamicParameters parameters)
    {
        StringBuilder builder = new StringBuilder();

        foreach (var keyval in request.EnumerateObject())
        {
            var property = schematic.Properties.Where(property => Guid.TryParse(keyval.Name, out var uuid) && property.Id == uuid).FirstOrDefault();
            if (property == null) continue;

            if (builder.Length > 0) builder.Append("AND ");
            builder.Append($"{property.ColumnName} LIKE CONCAT('%', @{property.ColumnName}, '%') ");
            parameters.Add($"@{property.ColumnName}", keyval.Value.GetString());
        }

        if (builder.Length == 0)
        {
            return string.Empty;
        }

        return "WHERE " + builder.ToString();
    }
}
