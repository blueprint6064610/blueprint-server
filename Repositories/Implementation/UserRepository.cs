using Blueprint.Data;
using Blueprint.Models;
using Dapper;
using System.Text;

namespace Blueprint.Repositories;

/// <summary>
/// Class UserRepository.
/// </summary>
public class UserRepository : IUserRepository
{
    /// <summary>
    /// The blueprint context.
    /// </summary>
    private readonly BlueprintContext context;

    /// <summary>
    /// UserRepository constructor.
    /// </summary>
    public UserRepository(BlueprintContext context)
    {
        this.context = context;
    }

    /// <inheritdoc/>
    public async Task Create(User user)
    {
        var parameters = new DynamicParameters();
        var createSql = GetCreateSql(user, parameters);

        using (var connection = context.CreateConnection())
        {
            await connection.ExecuteAsync(createSql, parameters);
        }
    }

    /// <inheritdoc/>
    public async Task Delete(Guid id)
    {
        var parameters = new DynamicParameters();
        var deleteSql = GetDeleteSql(id, parameters);

        using (var connection = context.CreateConnection())
        {
            await connection.ExecuteAsync(deleteSql, parameters);
        }
    }

    /// <inheritdoc/>
    public async Task<QueryResponse<User>> Find(UserFindRequest request)
    {
        var parameters = new DynamicParameters();
        var whereClause = GetFindWhereSql(request, parameters);
        var findTotalSql = GetFindTotalSql(whereClause);
        var findSql = GetFindSql(request, whereClause, parameters);

        using (var connection = context.CreateConnection())
        {
            var total = await connection.QuerySingleAsync<int>(findTotalSql, parameters);
            var results = await connection.QueryAsync<User>(findSql, parameters);
            return new QueryResponse<User>(total, results);
        }
    }

    /// <inheritdoc/>
    public async Task<User?> FindByEmail(string email)
    {
        var parameters = new DynamicParameters();
        var findByEmailSql = GetFindByEmailSql(email, parameters);

        using (var connection = context.CreateConnection())
        {
            return await connection.QuerySingleOrDefaultAsync<User>(findByEmailSql, parameters);
        }
    }

    /// <inheritdoc/>
    public async Task<User?> FindById(Guid id)
    {
        var parameters = new DynamicParameters();
        var findByIdSql = GetFindByIdSql(id, parameters);

        using (var connection = context.CreateConnection())
        {
            return await connection.QuerySingleOrDefaultAsync<User>(findByIdSql, parameters);
        }
    }

    /// <inheritdoc/>
    public async Task Update(User user)
    {
        var parameters = new DynamicParameters();
        var updateSql = GetUpdateSql(user, parameters);

        using (var connection = context.CreateConnection())
        {
            await connection.ExecuteAsync(updateSql, parameters);
        }
    }

    private string GetCreateSql(User user, DynamicParameters parameters)
    {
        var sql = @"
            INSERT INTO blueprint.user
                (first_name, last_name, email, password_hash, password_salt)
            VALUES
                (@FirstName, @LastName, @Email, @PasswordHash, @PasswordSalt)
        ";

        parameters.Add("@FirstName", user.FirstName);
        parameters.Add("@LastName", user.LastName);
        parameters.Add("@Email", user.Email);
        parameters.Add("@PasswordHash", user.PasswordHash);
        parameters.Add("@PasswordSalt", user.PasswordSalt);

        return sql;
    }

    private string GetDeleteSql(Guid id, DynamicParameters parameters)
    {
        var sql = @"DELETE FROM blueprint.user WHERE id = @Id";

        parameters.Add("@Id", id);

        return sql;
    }

    private string GetFindByEmailSql(string email, DynamicParameters parameters)
    {
        var sql = @"
            SELECT
                id AS Id,
                first_name AS FirstName,
                last_name AS LastName,
                email AS Email,
                password_hash AS PasswordHash,
                password_salt AS PasswordSalt
            FROM
                blueprint.user
            WHERE
                email = @Email
        ";

        parameters.Add("@Email", email);

        return sql;
    }

    private string GetFindByIdSql(Guid id, DynamicParameters parameters)
    {
        var sql = @"
            SELECT
                id AS Id,
                first_name AS FirstName,
                last_name AS LastName,
                email AS Email,
                password_hash AS PasswordHash,
                password_salt AS PasswordSalt
            FROM
                blueprint.user
            WHERE
                id = @Id
        ";

        parameters.Add("@Id", id);

        return sql;
    }

    private string GetFindSql(UserFindRequest request, string whereClause, DynamicParameters parameters)
    {
        var sql = @$"
            SELECT
                id AS Id,
                first_name AS FirstName,
                last_name AS LastName,
                email AS Email
            FROM
                blueprint.user
            {whereClause}
            LIMIT @Limit OFFSET @Offset
        ";

        parameters.Add("@Limit", request.PageSize);
        parameters.Add("@Offset", request.Page * request.PageSize);

        return sql;
    }

    private string GetFindTotalSql(string whereClause)
    {
        return @$"
            SELECT
                COUNT(*)
            FROM
                blueprint.user
            {whereClause}
        ";
    }

    /// <summary>
    /// Build the where clause for a user find request.
    /// </summary>
    /// <param name="request">The user find request.</param>
    /// <param name="parameters">The SQL parameters.</param>
    /// <returns>The where clause.</returns>
    private string GetFindWhereSql(UserFindRequest request, DynamicParameters parameters)
    {
        StringBuilder builder = new StringBuilder();

        if (!string.IsNullOrEmpty(request.FirstName))
        {
            builder.Append("first_name LIKE CONCAT('%', @FirstName, '%') ");
            parameters.Add("@FirstName", request.FirstName);
        }

        if (!string.IsNullOrEmpty(request.LastName))
        {
            if (builder.Length > 0) builder.Append("AND ");
            builder.Append("last_name LIKE CONCAT('%', @LastName, '%') ");
            parameters.Add("@LastName", request.LastName);
        }

        if (!string.IsNullOrEmpty(request.Email))
        {
            if (builder.Length > 0) builder.Append("AND ");
            builder.Append("email LIKE CONCAT('%', @Email, '%') ");
            parameters.Add("@Email", request.Email);
        }

        if (builder.Length == 0)
        {
            return string.Empty;
        }

        return "WHERE " + builder.ToString();
    }

    private string GetUpdateSql(User user, DynamicParameters parameters)
    {
        var sql = @"
            UPDATE blueprint.user SET
                first_name = @FirstName,
                last_name = @LastName,
                email = @Email,
                password_hash = @PasswordHash,
                password_salt = @PasswordSalt
            WHERE
                id = @Id
        ";

        parameters.Add("@FirstName", user.FirstName);
        parameters.Add("@LastName", user.LastName);
        parameters.Add("@Email", user.Email);
        parameters.Add("@PasswordHash", user.PasswordHash);
        parameters.Add("@PasswordSalt", user.PasswordSalt);
        parameters.Add("@Id", user.Id);

        return sql;
    }
}
