using Blueprint.Data;
using Blueprint.Models;
using Dapper;

namespace Blueprint.Repositories;

/// <summary>
/// Interface IPropertyTypeRepository.
/// </summary>
public class PropertyTypeRepository : IPropertyTypeRepository
{
    /// <summary>
    /// The blueprint context.
    /// </summary>
    private readonly BlueprintContext context;

    /// <summary>
    /// PropertyTypeRepository constructor.
    /// </summary>
    public PropertyTypeRepository(BlueprintContext context)
    {
        this.context = context;
    }

    /// <inheritdoc/>
    public async Task<QueryResponse<PropertyType>> Find(QueryRequest request)
    {
        var parameters = new DynamicParameters();
        var totalSql = GetTotalSql();
        var findSql = GetFindSql(request, parameters);

        using (var connection = context.CreateConnection())
        {
            var total = await connection.QuerySingleAsync<int>(totalSql);
            var results = await connection.QueryAsync<PropertyType>(findSql, parameters);
            return new QueryResponse<PropertyType>(total, results);
        }
    }

    /// <inheritdoc/>
    public async Task<PropertyType> FindByCode(string code)
    {
        var parameters = new DynamicParameters();
        var findByCodeSql = GetFindByCodeSql(code, parameters);

        using (var connection = context.CreateConnection())
        {
            return await connection.QuerySingleOrDefaultAsync<PropertyType>(findByCodeSql, parameters);
        }
    }

    /// <inheritdoc/>
    public async Task<PropertyType?> FindById(Guid id)
    {
        var parameters = new DynamicParameters();
        var findByIdSql = GetFindByIdSql(id, parameters);

        using (var connection = context.CreateConnection())
        {
            return await connection.QuerySingleOrDefaultAsync<PropertyType>(findByIdSql, parameters);
        }
    }

    private string GetTotalSql()
    {
        return "SELECT COUNT(*) FROM blueprint.property_type";
    }

    private string GetFindByCodeSql(string code, DynamicParameters parameters)
    {
        var sql = @"
            SELECT
                id AS Id,
                code AS Code,
                name AS Name,
                description AS Description
            FROM
                blueprint.property_type
            WHERE
                code = @Code
        ";

        parameters.Add("@Code", code);

        return sql;
    }

    private string GetFindByIdSql(Guid id, DynamicParameters parameters)
    {
        var sql = @"
            SELECT
                id AS Id,
                code AS Code,
                name AS Name,
                description AS Description
            FROM
                blueprint.property_type
            WHERE
                id = @Id
        ";

        parameters.Add("@Id", id);

        return sql;
    }

    private string GetFindSql(QueryRequest request, DynamicParameters parameters)
    {
        var sql = @"
            SELECT
                id AS Id,
                name AS Name,
                description AS Description
            FROM
                blueprint.property_type
            LIMIT @Limit OFFSET @Offset
        ";

        parameters.Add("@Limit", request.PageSize);
        parameters.Add("@Offset", request.Page * request.PageSize);

        return sql;
    }
}
