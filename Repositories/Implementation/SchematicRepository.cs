using Blueprint.Data;
using Blueprint.Models;
using Dapper;
using System.Text;

namespace Blueprint.Repositories;

/// <summary>
/// Class SchematicRepository.
/// </summary>
public class SchematicRepository : ISchematicRepository
{
    /// <summary>
    /// The blueprint context.
    /// </summary>
    private readonly BlueprintContext context;

    /// <summary>
    /// SchematicRepository constructor.
    /// </summary>
    public SchematicRepository(BlueprintContext context)
    {
        this.context = context;
    }

    /// <inheritdoc/>
    public async Task Create(Schematic schematic)
    {
        var parameters = new DynamicParameters();
        var createSql = GetCreateSql(schematic, parameters);
        var createTableSql = GetCreateTableSql(schematic);

        using (var connection = context.CreateConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                await connection.ExecuteAsync(createSql, parameters, transaction);
                await connection.ExecuteAsync(createTableSql, transaction: transaction);
                transaction.Commit();
            }
        }
    }

    /// <inheritdoc/>
    public async Task Delete(Guid id)
    {
        var schematic = await FindById(id);
        if (schematic == null) return;

        var parameters = new DynamicParameters();
        var deletePropertiesSql = GetDeletePropertiesSql(id, parameters);
        var deleteSchematicSql = GetDeleteSchematicSql(id, parameters);
        var deleteTableSql = GetDeleteTableSql(schematic.TableName, parameters);

        using (var connection = context.CreateConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                await connection.ExecuteAsync(deletePropertiesSql, parameters, transaction);
                await connection.ExecuteAsync(deleteSchematicSql, parameters, transaction);
                await connection.ExecuteAsync(deleteTableSql, transaction: transaction);
                transaction.Commit();
            }
        }
    }

    /// <inheritdoc/>
    public async Task<QueryResponse<Schematic>> Find(SchematicFindRequest request)
    {
        var parameters = new DynamicParameters();
        var whereClause = GetFindWhereSql(request, parameters);
        var findTotalSql = GetFindTotalSql(whereClause);
        var findSql = GetFindSql(request, whereClause, parameters);

        using (var connection = context.CreateConnection())
        {
            var total = await connection.QuerySingleAsync<int>(findTotalSql, parameters);
            var results = await connection.QueryAsync<Schematic>(findSql, parameters);
            return new QueryResponse<Schematic>(total, results);
        }
    }

    /// <inheritdoc/>
    public async Task<Schematic?> FindById(Guid id)
    {
        var parameters = new DynamicParameters();
        var findByIdSql = GetFindByIdSql(id, parameters);

        using (var connection = context.CreateConnection())
        {
            Schematic? root = null;
            var mapper = (Schematic? schematic, Property? property, PropertyType? propertyType) =>
            {
                root ??= schematic;

                if (root != null && property != null && propertyType != null)
                {
                    property.PropertyType = propertyType;
                    root.Properties.Add(property);
                }

                return root;
            };

            var results = await connection.QueryAsync<Schematic, Property, PropertyType, Schematic>(findByIdSql, mapper, parameters);

            return results.FirstOrDefault();
        }
    }

    /// <inheritdoc/>
    public async Task Update(Schematic schematic)
    {
        var oldSchematic = await FindById(schematic.Id);
        if (oldSchematic == null) return;

        var parameters = new DynamicParameters();
        var updateSql = GetUpdateSql(schematic, parameters);

        using (var connection = context.CreateConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                await connection.ExecuteAsync(updateSql, parameters, transaction);
                if (oldSchematic.TableName != schematic.TableName)
                {
                    var renameTableSql = GetRenameTableSql(oldSchematic.TableName, schematic.TableName);
                    await connection.ExecuteAsync(renameTableSql, transaction: transaction);
                }
                transaction.Commit();
            }
        }
    }

    private string GetCreateSql(Schematic schematic, DynamicParameters parameters)
    {
        var sql = @"
            INSERT INTO blueprint.schematic
                (name, description, table_name)
            VALUES
                (@Name, @Description, @TableName)
        ";

        parameters.Add("@Name", schematic.Name);
        parameters.Add("@Description", schematic.Description);
        parameters.Add("@TableName", schematic.TableName);

        return sql;
    }

    private string GetCreateTableSql(Schematic schematic)
    {
        return @$"
            CREATE TABLE custom.{schematic.TableName}
            (
                id UUID NOT NULL DEFAULT uuid_generate_v4(),
                PRIMARY KEY (id)
            )
        ";
    }

    private string GetDeleteSchematicSql(Guid id, DynamicParameters parameters)
    {
        var sql = "DELETE FROM blueprint.schematic WHERE id = @Id";

        parameters.Add("@Id", id);

        return sql;
    }

    private string GetDeletePropertiesSql(Guid id, DynamicParameters parameters)
    {
        var sql = "DELETE FROM blueprint.property WHERE schematic_id = @Id";

        parameters.Add("@Id", id);

        return sql;
    }

    private string GetDeleteTableSql(string tableName, DynamicParameters parameters)
    {
        return $"DROP TABLE custom.{tableName}";
    }

    private string GetFindSql(SchematicFindRequest request, string whereClause, DynamicParameters parameters)
    {
        var sql = @$"
            SELECT
                id AS Id,
                name AS Name,
                description AS Description
            FROM
                blueprint.schematic
            {whereClause}
            LIMIT @Limit OFFSET @Offset
        ";

        parameters.Add("@Limit", request.PageSize);
        parameters.Add("@Offset", request.Page * request.PageSize);

        return sql;
    }

    private string GetFindTotalSql(string whereClause)
    {
        return $"SELECT COUNT(*) FROM blueprint.schematic {whereClause}";
    }

    private string GetFindByIdSql(Guid id, DynamicParameters parameters)
    {
        var sql = @"
            SELECT
                s.id AS Id,
                s.name AS Name,
                s.description AS Description,
                s.table_name AS TableName,
                p.id AS Id,
                p.schematic_id AS SchematicId,
                p.name AS Name,
                p.description AS Description,
                p.column_name AS ColumnName,
                p.view_order AS ViewOrder,
                p.property_type_id AS PropertyTypeId,
                pt.id AS Id,
                pt.code AS Code,
                pt.name AS Name,
                pt.description AS Description
            FROM
                blueprint.schematic s
                LEFT JOIN blueprint.property p ON s.id = p.schematic_id
                LEFT JOIN blueprint.property_type pt ON p.property_type_id = pt.id
            WHERE
                s.id = @Id
            ORDER BY
                p.view_order
        ";

        parameters.Add("@Id", id);

        return sql;
    }

    /// <summary>
    /// Build the where clause for a schematic find request.
    /// </summary>
    /// <param name="request">The schematic find request.</param>
    /// <param name="parameters">The SQL parameters.</param>
    /// <returns>The where clause.</returns>
    private string GetFindWhereSql(SchematicFindRequest request, DynamicParameters parameters)
    {
        StringBuilder builder = new StringBuilder();

        if (!string.IsNullOrEmpty(request.Name))
        {
            builder.Append("name LIKE CONCAT('%', @Name, '%') ");
            parameters.Add("@Name", request.Name);
        }

        if (!string.IsNullOrEmpty(request.Description))
        {
            if (builder.Length > 0) builder.Append("AND ");
            builder.Append("description LIKE CONCAT('%', @Description, '%') ");
            parameters.Add("@Description", request.Description);
        }

        if (builder.Length == 0)
        {
            return string.Empty;
        }

        return "WHERE " + builder.ToString();
    }

    private string GetUpdateSql(Schematic schematic, DynamicParameters parameters)
    {
        var sql = @"
            UPDATE
                blueprint.schematic
            SET
                name = @Name,
                description = @Description,
                table_name = @TableName
            WHERE
                id = @Id
        ";

        parameters.Add("@Name", schematic.Name);
        parameters.Add("@Description", schematic.Description);
        parameters.Add("@TableName", schematic.TableName);
        parameters.Add("@Id", schematic.Id);

        return sql;
    }

    private string GetRenameTableSql(string oldTableName, string newTableName)
    {
        return $"ALTER TABLE custom.{oldTableName} RENAME TO {newTableName}";
    }
}
