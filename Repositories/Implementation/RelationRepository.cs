using Blueprint.Data;
using Blueprint.Models;
using Dapper;

namespace Blueprint.Repositories;

/// <summary>
/// Class RelationRepository.
/// </summary>
public class RelationRepository : IRelationRepository
{
    /// <summary>
    /// The database context.
    /// </summary>
    private readonly BlueprintContext context;

    /// <summary>
    /// RelationRepository constructor.
    /// </summary>
    public RelationRepository(BlueprintContext context)
    {
        this.context = context;
    }

    /// <inheritdoc/>
    public async Task Create(Relation relation)
    {
        var parameters = new DynamicParameters();
        var createSql = GetCreateSql(relation, parameters);
        var createJoinTableSql = GetCreateJoinTableSql(relation);

        using (var connection = context.CreateConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                await connection.ExecuteAsync(createSql, parameters, transaction);
                await connection.ExecuteAsync(createJoinTableSql, transaction: transaction);
                transaction.Commit();
            }
        }
    }

    /// <inheritdoc/>
    public async Task Delete(Relation relation)
    {
        var parameters = new DynamicParameters();
        var deleteSql = GetDeleteSql(relation, parameters);
        var deleteJoinTableSql = GetDeleteJoinTableSql(relation);

        using (var connection = context.CreateConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                await connection.ExecuteAsync(deleteSql, parameters, transaction);
                await connection.ExecuteAsync(deleteJoinTableSql, transaction: transaction);
                transaction.Commit();
            }
        }
    }

    /// <inheritdoc/>
    public async Task<Relation?> FindById(Guid id)
    {
        var parameters = new DynamicParameters();
        var findByIdSql = GetFindByIdSql(id, parameters);

        using (var connection = context.CreateConnection())
        {
            var mapper = (Relation relation, Schematic lhsSchematic, Schematic rhsSchematic) =>
            {
                if (relation == null) return null;
                relation.LhsSchematic = lhsSchematic;
                relation.RhsSchematic = rhsSchematic;
                return relation;
            };

            var result = await connection.QueryAsync<Relation, Schematic, Schematic, Relation>(findByIdSql, mapper, parameters);
            return result.FirstOrDefault();
        }
    }

    /// <inheritdoc/>
    public async Task<QueryResponse<Relation>> FindBySchematic(Guid id, QueryRequest request)
    {
        var parameters = new DynamicParameters();
        var findBySchematicTotalSql = GetFindBySchematicTotalSql(id, parameters);
        var findBySchematicSql = GetFindBySchematicSql(id, request, parameters);

        using (var connection = context.CreateConnection())
        {
            var total = await connection.QuerySingleAsync<int>(findBySchematicTotalSql, parameters);
            var results = await connection.QueryAsync<Relation>(findBySchematicSql, parameters);
            return new QueryResponse<Relation>(total, results);
        }
    }

    /// <summary>
    /// Generate the SQL to create the relation.
    /// </summary>
    /// <param name="relation">The relation.</param>
    /// <param name="parameters">The query parameters.</param>
    /// <returns>The SQL.</returns>
    private string GetCreateSql(Relation relation, DynamicParameters parameters)
    {
        var sql = @"
            INSERT INTO blueprint.relation
                (name, description, join_table_name, lhs_schematic_id, rhs_schematic_id, relation_type)
            VALUES
                (@Name, @Description, @JoinTableName, @LhsSchematicId, @RhsSchematicId, @RelationType::relation_type)
        ";

        parameters.Add("@Name", relation.Name);
        parameters.Add("@Description", relation.Description);
        parameters.Add("@JoinTableName", relation.JoinTableName);
        parameters.Add("@LhsSchematicId", relation.LhsSchematicId);
        parameters.Add("@RhsSchematicId", relation.RhsSchematicId);
        parameters.Add("@RelationType", relation.RelationType.ToString());

        return sql;
    }

    /// <summary>
    /// Generate the SQL to create the join table.
    /// </summary>
    /// <param name="relation">The relation.</param>
    /// <returns>The SQL.</returns>
    private string GetCreateJoinTableSql(Relation relation)
    {
        string uniqueConstraint = string.Empty;
        if (relation.RelationType == RelationType.OneToOne)
        {
            uniqueConstraint = @$"
                UNIQUE ({relation.LhsSchematic.TableName}_id),
                UNIQUE ({relation.RhsSchematic.TableName}_id),
            ";
        }
        else if (relation.RelationType == RelationType.OneToMany)
        {
            uniqueConstraint = @$"
                UNIQUE ({relation.RhsSchematic.TableName}_id),
            ";
        }
        else if (relation.RelationType == RelationType.ManyToOne)
        {
            uniqueConstraint = @$"
                UNIQUE ({relation.LhsSchematic.TableName}_id),
            ";
        }
        else if (relation.RelationType == RelationType.ManyToMany)
        {
            uniqueConstraint = @$"
                UNIQUE ({relation.LhsSchematic.TableName}_id, {relation.RhsSchematic.TableName}_id),
            ";
        }

        return @$"
            CREATE TABLE custom.{relation.JoinTableName}
            (
                id UUID NOT NULL DEFAULT uuid_generate_v4(),
                {relation.LhsSchematic.TableName}_id UUID NOT NULL,
                {relation.RhsSchematic.TableName}_id UUID NOT NULL,
                PRIMARY KEY (id),
                {uniqueConstraint}
                FOREIGN KEY ({relation.LhsSchematic.TableName}_id) REFERENCES custom.{relation.LhsSchematic.TableName} (id),
                FOREIGN KEY ({relation.RhsSchematic.TableName}_id) REFERENCES custom.{relation.RhsSchematic.TableName} (id)
            )
        ";
    }

    /// <summary>
    /// Generate the SQL to delete the relation.
    /// </summary>
    /// <param name="relation">The relation.</param>
    /// <param name="parameters">The query parameters.</param>
    /// <returns>The SQL.</returns>
    private string GetDeleteSql(Relation relation, DynamicParameters parameters)
    {
        var sql = "DELETE FROM blueprint.relation WHERE id = @Id";

        parameters.Add("@Id", relation.Id);

        return sql;
    }

    /// <summary>
    /// Generate the SQL to delete the join table.
    /// </summary>
    /// <param name="relation">The relation.</param>
    /// <returns>The SQL.</returns>
    private string GetDeleteJoinTableSql(Relation relation)
    {
        return $"DROP TABLE custom.{relation.JoinTableName}";
    }

    /// <summary>
    /// Generate the find by id SQL.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <param name="parameters">The query parameters.</param>
    /// <returns>The SQL.</returns>
    private string GetFindByIdSql(Guid id, DynamicParameters parameters)
    {
        var sql = @$"
            SELECT
                r.id AS Id,
                r.name AS Name,
                r.description AS Description,
                r.join_table_name AS JoinTableName,
                r.lhs_schematic_id AS LhsSchematicId,
                r.rhs_schematic_id AS RhsSchematicId,
                r.relation_type AS RelationType,
                ls.id AS Id,
                ls.name AS Name,
                ls.description AS Description,
                ls.table_name AS TableName,
                rs.id AS Id,
                rs.name AS Name,
                rs.description AS Description,
                rs.table_name AS TableName
            FROM
                blueprint.relation r
                INNER JOIN blueprint.schematic ls ON r.lhs_schematic_id = ls.id
                INNER JOIN blueprint.schematic rs ON r.rhs_schematic_id = rs.id
            WHERE
                r.id = @Id
        ";

        parameters.Add("@Id", id);

        return sql;
    }

    /// <summary>
    /// Generate the find by schematic SQL.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <param name="parameters">The query parameters.</param>
    /// <returns>The SQL.</returns>
    private string GetFindBySchematicSql(Guid id, QueryRequest request, DynamicParameters parameters)
    {
        var sql = @$"
            SELECT
                r.id AS Id,
                r.name AS Name,
                r.description AS Description,
                r.join_table_name AS JoinTableName,
                r.lhs_schematic_id AS LhsSchematicId,
                r.rhs_schematic_id AS RhsSchematicId,
                r.relation_type AS RelationType
            FROM
                blueprint.relation r
            WHERE
                r.lhs_schematic_id = @Id
                OR r.rhs_schematic_id = @Id
            ORDER BY
                r.name
            LIMIT @Limit OFFSET @Offset
        ";

        parameters.Add("@Id", id);
        parameters.Add("@Limit", request.PageSize);
        parameters.Add("@Offset", request.Page * request.PageSize);

        return sql;
    }

    /// <summary>
    /// Generate the find by schematic total SQL.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <param name="parameters">The query parameters.</param>
    /// <returns>The SQL.</returns>
    private string GetFindBySchematicTotalSql(Guid id, DynamicParameters parameters)
    {
        var sql = @$"
            SELECT
                COUNT(*)
            FROM
                blueprint.relation r
            WHERE
                r.lhs_schematic_id = @Id
                OR r.rhs_schematic_id = @Id
        ";

        parameters.Add("@Id", id);

        return sql;
    }
}
