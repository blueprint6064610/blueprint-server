using Blueprint.Data;
using Blueprint.Models;
using Blueprint.Services;
using Dapper;

namespace Blueprint.Repositories;

/// <summary>
/// Class PropertyRepository.
/// </summary>
public class PropertyRepository : IPropertyRepository
{
    /// <summary>
    /// The context.
    /// </summary>
    private readonly BlueprintContext context;

    /// <summary>
    /// The SQL type resolver.
    /// </summary>
    private readonly ISqlTypeResolver sqlTypeResolver;

    /// <summary>
    /// PropertyRepository constructor.
    /// </summary>
    public PropertyRepository(BlueprintContext context, ISqlTypeResolver sqlTypeResolver)
    {
        this.context = context;
        this.sqlTypeResolver = sqlTypeResolver;
    }

    /// <inheritdoc/>
    public async Task Create(Property property)
    {
        var parameters = new DynamicParameters();
        var createPropertySql = GetCreatePropertySql(property, parameters);
        var createColumnSql = GetCreateColumnSql(property);

        using (var connection = context.CreateConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                await connection.ExecuteAsync(createPropertySql, parameters, transaction);
                await connection.ExecuteAsync(createColumnSql, transaction: transaction);
                transaction.Commit();
            }
        }
    }

    /// <inheritdoc/>
    public async Task Delete(Property property)
    {
        var parameters = new DynamicParameters();
        var deletePropertySql = GetDeletePropertySql(property.Id, parameters);
        var deleteColumnSql = GetDeleteColumnSql(property);

        using (var connection = context.CreateConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                await connection.ExecuteAsync(deletePropertySql, parameters, transaction);
                await connection.ExecuteAsync(deleteColumnSql, transaction: transaction);
                transaction.Commit();
            }
        }
    }

    /// <inheritdoc/>
    public async Task<Property?> FindById(Guid id)
    {
        var parameters = new DynamicParameters();
        var findByIdSql = GetFindByIdSql(id, parameters);

        using (var connection = context.CreateConnection())
        {
            var mapper = (Property property, PropertyType propertyType) =>
            {
                property.PropertyType = propertyType;
                return property;
            };

            var results = await connection.QueryAsync<Property, PropertyType, Property>(findByIdSql, mapper, parameters);

            return results.FirstOrDefault();
        }
    }

    /// <inheritdoc/>
    public async Task<QueryResponse<Property>> FindBySchematic(Guid schematicId, QueryRequest request)
    {
        var parameters = new DynamicParameters();
        var findBySchematicTotalSql = GetFindBySchematicTotalSql(schematicId, parameters);
        var findBySchematicSql = GetFindBySchematicSql(schematicId, request, parameters);

        using (var connection = context.CreateConnection())
        {
            var mapper = (Property property, PropertyType propertyType) =>
            {
                property.PropertyType = propertyType;
                return property;
            };

            var total = await connection.QuerySingleAsync<int>(findBySchematicTotalSql, parameters);
            var results = await connection.QueryAsync<Property, PropertyType, Property>(findBySchematicSql, mapper, parameters);
            return new QueryResponse<Property>(total, results);
        }
    }

    /// <inheritdoc/>
    public async Task Update(Property property)
    {
        var oldProperty = await FindById(property.Id);
        if (oldProperty == null) return;

        var parameters = new DynamicParameters();
        var updatePropertySql = GetUpdatePropertySql(property, parameters);

        using (var connection = context.CreateConnection())
        {
            connection.Open();
            using (var transaction = connection.BeginTransaction())
            {
                await connection.ExecuteAsync(updatePropertySql, parameters, transaction);
                if (oldProperty.ColumnName != property.ColumnName)
                {
                    var tableName = property.Schematic.TableName;
                    var oldColumnName = oldProperty.ColumnName;
                    var newColumnName = property.ColumnName;
                    var updateColumnSql = GetUpdateColumnSql(tableName, oldColumnName, newColumnName);
                    await connection.ExecuteAsync(updateColumnSql, transaction: transaction);
                }
                transaction.Commit();
            }
        }
    }

    private string GetCreatePropertySql(Property property, DynamicParameters parameters)
    {
        var sql = @"
            INSERT INTO blueprint.property
                (schematic_id, name, description, column_name, view_order, property_type_id)
            VALUES
                (@SchematicId, @Name, @Description, @ColumnName, @ViewOrder, @PropertyTypeId)
        ";

        parameters.Add("@SchematicId", property.SchematicId);
        parameters.Add("@Name", property.Name);
        parameters.Add("@Description", property.Description);
        parameters.Add("@ColumnName", property.ColumnName);
        parameters.Add("@ViewOrder", property.ViewOrder);
        parameters.Add("@PropertyTypeId", property.PropertyTypeId);

        return sql;
    }

    private string GetCreateColumnSql(Property property)
    {
        var tableName = property.Schematic.TableName;
        var columnName = property.ColumnName;
        var type = sqlTypeResolver.Resolve(property.PropertyType);
        return $"ALTER TABLE custom.{tableName} ADD COLUMN {columnName} {type}";
    }

    private string GetDeletePropertySql(Guid id, DynamicParameters parameters)
    {
        var sql = "DELETE FROM blueprint.property WHERE id = @Id";

        parameters.Add("@Id", id);

        return sql;
    }

    private string GetDeleteColumnSql(Property property)
    {
        var tableName = property.Schematic.TableName;
        var columnName = property.ColumnName;
        return $"ALTER TABLE custom.{tableName} DROP COLUMN {columnName}";
    }

    private string GetFindByIdSql(Guid id, DynamicParameters parameters)
    {
        var sql = @"
            SELECT
                p.id AS Id,
                p.schematic_id AS SchematicId,
                p.name AS Name,
                p.description AS Description,
                p.column_name AS ColumnName,
                p.view_order AS ViewOrder,
                p.property_type_id AS PropertyTypeId,
                pt.id AS Id,
                pt.code AS Code,
                pt.name AS Name,
                pt.description AS Description
            FROM
                blueprint.property p
                LEFT JOIN blueprint.property_type pt ON p.property_type_id = pt.id
            WHERE
                p.id = @Id
        ";

        parameters.Add("@Id", id);

        return sql;
    }

    private string GetFindBySchematicSql(Guid schematicId, QueryRequest request, DynamicParameters parameters)
    {
        var sql = @"
            SELECT
                p.id AS Id,
                p.schematic_id AS SchematicId,
                p.name AS Name,
                p.description AS Description,
                p.column_name AS ColumnName,
                p.view_order AS ViewOrder,
                p.property_type_id AS PropertyTypeId,
                pt.id AS Id,
                pt.code AS Code,
                pt.name AS Name,
                pt.description AS Description
            FROM
                blueprint.property p
                LEFT JOIN blueprint.property_type pt ON p.property_type_id = pt.id
            WHERE
                p.schematic_id = @SchematicId
            ORDER BY
                p.view_order
            LIMIT @Limit OFFSET @Offset
        ";

        parameters.Add("@SchematicId", schematicId);
        parameters.Add("@Limit", request.PageSize);
        parameters.Add("@Offset", request.Page * request.PageSize);

        return sql;
    }

    private string GetFindBySchematicTotalSql(Guid schematicId, DynamicParameters parameters)
    {
        var sql = "SELECT COUNT(*) FROM blueprint.property WHERE schematic_id = @SchematicId";

        parameters.Add("@SchematicId", schematicId);

        return sql;
    }

    private string GetUpdatePropertySql(Property property, DynamicParameters parameters)
    {
        var sql = @"
            UPDATE
                blueprint.property
            SET
                name = @Name,
                description = @Description,
                column_name = @ColumnName
            WHERE
                id = @Id
        ";

        parameters.Add("@Name", property.Name);
        parameters.Add("@Description", property.Description);
        parameters.Add("@ColumnName", property.ColumnName);
        parameters.Add("@Id", property.Id);

        return sql;
    }

    private string GetUpdateColumnSql(string tableName, string oldColumnName, string newColumnName)
    {
        return $"ALTER TABLE custom.{tableName} RENAME COLUMN {oldColumnName} TO {newColumnName}";
    }
}
