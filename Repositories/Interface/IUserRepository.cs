using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Repositories;

/// <summary>
/// Interface IUserRepository.
/// </summary>
public interface IUserRepository
{
    /// <summary>
    /// Create a user.
    /// </summary>
    /// <param name="user">The user.</param>
    /// <returns>If successful.</returns>
    Task Create(User user);

    /// <summary>
    /// Delete a user.
    /// </summary>
    /// <param name="id">The user id.</param>
    /// <returns>If successful.</returns>
    Task Delete(Guid id);

    /// <summary>
    /// Find users based on a query.
    /// </summary>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<User>> Find(UserFindRequest request);

    /// <summary>
    /// Find a user by email.
    /// </summary>
    /// <param name="email">The user email.</param>
    /// <returns>The user if found.</returns>
    Task<User?> FindByEmail(string email);

    /// <summary>
    /// Find a user by id.
    /// </summary>
    /// <param name="id">The user id.</param>
    /// <returns>The user if found.</returns>
    Task<User?> FindById(Guid id);

    /// <summary>
    /// Update a user.
    /// </summary>
    /// <param name="user">The user.</param>
    /// <returns>If successful.</returns>
    Task Update(User user);
}
