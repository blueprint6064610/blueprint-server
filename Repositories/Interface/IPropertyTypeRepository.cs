using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Repositories;

/// <summary>
/// Interface IPropertyTypeRepository.
/// </summary>
public interface IPropertyTypeRepository
{
    /// <summary>
    /// Find property types based on a query.
    /// </summary>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<PropertyType>> Find(QueryRequest request);

    /// <summary>
    /// Find a property type by id.
    /// </summary>
    /// <param name="id">The property type id.</param>
    /// <returns>The property type if found.</returns>
    Task<PropertyType?> FindById(Guid id);

    /// <summary>
    /// Find a property type by code.
    /// </summary>
    /// <param name="code">The property type code.</param>
    /// <returns>The property type if found.</returns>
    Task<PropertyType> FindByCode(string code);
}
