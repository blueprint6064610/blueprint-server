using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Repositories;

/// <summary>
/// Class IPropertyRepository.
/// </summary>
public interface IPropertyRepository
{
    Task Create(Property property);

    Task Delete(Property property);

    /// <summary>
    /// Find a property by id.
    /// </summary>
    /// <param name="id">The property id.</param>
    /// <returns>The property if found.</returns>
    Task<Property?> FindById(Guid id);

    Task<QueryResponse<Property>> FindBySchematic(Guid schematicId, QueryRequest request);

    /// <summary>
    /// Update a property.
    /// </summary>
    /// <param name="property">The property to update.</param>
    /// <returns>If successful.</returns>
    Task Update(Property property);
}
