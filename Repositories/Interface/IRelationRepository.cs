using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Repositories;

/// <summary>
/// Interface IRelationRepository.
/// </summary>
public interface IRelationRepository
{
    /// <summary>
    /// Create a relation.
    /// </summary>
    /// <param name="relation">The relation.</param>
    /// <returns>Task</returns>
    Task Create(Relation relation);

    /// <summary>
    /// Delete a relation.
    /// </summary>
    /// <param name="relation">The relation.</param>
    /// <returns>Task</returns>
    Task Delete(Relation relation);

    /// <summary>
    /// Find a relation by id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>The relation if found, else null.</returns>
    Task<Relation?> FindById(Guid id);

    /// <summary>
    /// Find relations by schematic.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<Relation>> FindBySchematic(Guid id, QueryRequest request);
}
