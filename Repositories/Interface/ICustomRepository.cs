using Blueprint.Data;
using Blueprint.Models;
using System.Text.Json;

namespace Blueprint.Repositories;

/// <summary>
/// Interface ICustomRepository.
/// </summary>
public interface ICustomRepository
{
    /// <summary>
    /// Create a custom entity.
    /// </summary>
    /// <param name="schematic">The schematic.</param>
    /// <param name="request">The create entity request.</param>
    /// <returns>If successful.</returns>
    Task Create(Schematic schematic, JsonElement request);

    /// <summary>
    /// Delete a custom entity.
    /// </summary>
    /// <param name="schematic">The schematic.</param>
    /// <param name="id">The custom entity id.</param>
    /// <returns>If successful.</returns>
    Task Delete(Schematic schematic, Guid id);

    /// <summary>
    /// Find custom entities based on a query.
    /// </summary>
    /// <param name="schematic">The schematic.</param>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<object>> Find(Schematic schematic, JsonElement request);

    /// <summary>
    /// Find a custom entity by id.
    /// </summary>
    /// <param name="schematic">The schematic.</param>
    /// <param name="id">The custom entity id.</param>
    /// <returns>The custom entity if found.</returns>
    Task<object?> FindById(Schematic schematic, Guid id);

    /// <summary>
    /// Update a custom entity.
    /// </summary>
    /// <param name="schematic">The entity schematic.</param>
    /// <param name="id">The entity id.</param>
    /// <param name="request">The update request.</param>
    /// <returns></returns>
    Task Update(Schematic schematic, Guid id, JsonElement request);
}
