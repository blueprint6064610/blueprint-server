using Blueprint.Data;
using Blueprint.Models;

namespace Blueprint.Repositories;

/// <summary>
/// Interface ISchematicRepository.
/// </summary>
public interface ISchematicRepository
{
    /// <summary>
    /// Create a schematic.
    /// </summary>
    /// <param name="schematic">The schematic.</param>
    /// <returns>If successful.</returns>
    Task Create(Schematic schematic);

    /// <summary>
    /// Delete a schematic.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <returns>If successful.</returns>
    Task Delete(Guid id);

    /// <summary>
    /// Find schematics based on a query.
    /// </summary>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    Task<QueryResponse<Schematic>> Find(SchematicFindRequest request);

    /// <summary>
    /// Find a schematic by id.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <returns>The schematic if found.</returns>
    Task<Schematic?> FindById(Guid id);

    /// <summary>
    /// Update the schematic.
    /// </summary>
    /// <param name="schematic">The schematic to update.</param>
    /// <returns>If successful.</returns>
    Task Update(Schematic schematic);
}
