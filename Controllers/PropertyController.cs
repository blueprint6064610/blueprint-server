using Blueprint.Models;
using Blueprint.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blueprint.Controllers;

/// <summary>
/// Class PropertyController.
/// </summary>
[ApiController]
[Authorize]
[Route("schematics/{schematicId}/properties")]
public class PropertyController : ControllerBase
{
    private readonly IPropertyService propertyService;

    private readonly ILogger<PropertyController> logger;

    public PropertyController(IPropertyService propertyService, ILogger<PropertyController> logger)
    {
        this.propertyService = propertyService;
        this.logger = logger;
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromRoute] Guid schematicId, [FromBody] PropertyCreateRequest request)
    {
        try
        {
            var success = await propertyService.Create(schematicId, request);
            return success ? Ok() : BadRequest();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete([FromRoute] Guid schematicId, Guid id)
    {
        try
        {
            var success = await propertyService.Delete(schematicId, id);
            return success ? Ok() : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpGet]
    public async Task<IActionResult> FindBySchematic([FromRoute] Guid schematicId, [FromQuery] QueryRequest request)
    {
        try
        {
            var results = await propertyService.FindBySchematic(schematicId, request);
            return Ok(results);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Update([FromRoute] Guid schematicId, [FromRoute] Guid id, [FromBody] PropertyUpdateRequest request)
    {
        try
        {
            var success = await propertyService.Update(schematicId, id, request);
            return success ? Ok() : BadRequest();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
