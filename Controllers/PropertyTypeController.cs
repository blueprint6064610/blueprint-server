using Blueprint.Models;
using Blueprint.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blueprint.Controllers;

/// <summary>
/// Class PropertyTypeController.
/// </summary>
[ApiController]
[Authorize]
[Route("property-types")]
public class PropertyTypeController : ControllerBase
{
    /// <summary>
    /// The property type service.
    /// </summary>
    private readonly IPropertyTypeService propertyTypeService;

    /// <summary>
    /// The logger.
    /// </summary>
    private readonly ILogger<PropertyTypeController> logger;

    /// <summary>
    /// PropertyTypeController constructor.
    /// </summary>
    public PropertyTypeController(IPropertyTypeService propertyTypeService, ILogger<PropertyTypeController> logger)
    {
        this.propertyTypeService = propertyTypeService;
        this.logger = logger;
    }

    /// <summary>
    /// Find property types that match the query.
    /// </summary>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    [HttpGet]
    public async Task<IActionResult> Find([FromQuery] QueryRequest request)
    {
        try
        {
            var propertyTypes = await propertyTypeService.Find(request);
            return Ok(propertyTypes);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Find a property type by id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>The property type if found.</returns>
    [HttpGet("{id}")]
    public async Task<IActionResult> FindById([FromRoute] Guid id)
    {
        try
        {
            var propertyType = await propertyTypeService.FindById(id);
            return propertyType != null ? Ok(propertyType) : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
