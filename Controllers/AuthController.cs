using Blueprint.Models;
using Blueprint.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blueprint.Controllers;

/// <summary>
/// Class AuthController.
/// </summary>
[ApiController]
[Route("auth")]
public class AuthController : ControllerBase
{
    /// <summary>
    /// The authentication service.
    /// </summary>
    private readonly IAuthService authService;

    /// <summary>
    /// The user service.
    /// </summary>
    private readonly IUserService userService;

    /// <summary>
    /// The logger.
    /// </summary>
    private readonly ILogger<AuthController> logger;

    /// <summary>
    /// AuthController constructor.
    /// </summary>
    public AuthController(IAuthService authService, IUserService userService, ILogger<AuthController> logger)
    {
        this.authService = authService;
        this.userService = userService;
        this.logger = logger;
    }

    /// <summary>
    /// Attempts to login.
    /// </summary>
    /// <param name="request">The login request.</param>
    /// <returns>The authentication token.</returns>
    [HttpPost("login")]
    [AllowAnonymous]
    public async Task<IActionResult> Login([FromBody] AuthLoginRequest request)
    {
        try
        {
            var validated = await authService.ValidateUser(request);
            if (!validated)
            {
                return Unauthorized("Invalid email or password");
            }

            var user = await userService.FindByEmail(request.Email);
            var token = await authService.GenerateToken(user!);
            var response = new AuthLoginResponse { Token = token };
            return Ok(response);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
