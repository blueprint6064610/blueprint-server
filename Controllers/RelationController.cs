using Blueprint.Models;
using Blueprint.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blueprint.Controllers;

/// <summary>
/// Class RelationController.
/// </summary>
[ApiController]
[Authorize]
public class RelationController : ControllerBase
{
    /// <summary>
    /// The relation service.
    /// </summary>
    private readonly IRelationService relationService;

    /// <summary>
    /// The logger.
    /// </summary>
    private readonly ILogger<RelationController> logger;

    /// <summary>
    /// RelationController constructor.
    /// </summary>
    public RelationController(IRelationService relationService, ILogger<RelationController> logger)
    {
        this.relationService = relationService;
        this.logger = logger;
    }

    /// <summary>
    /// Create a relation.
    /// </summary>
    /// <param name="request">The relation create request.</param>
    /// <returns>IActionResult</returns>
    [HttpPost("relations")]
    public async Task<IActionResult> Create([FromBody] RelationCreateRequest request)
    {
        try
        {
            var success = await relationService.Create(request);
            return success ? Ok() : BadRequest();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Get the relations for a schematic.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    [HttpGet("schematics/{id}/relations")]
    public async Task<IActionResult> FindBySchematic([FromRoute] Guid id, [FromQuery] QueryRequest request)
    {
        try
        {
            var response = await relationService.FindBySchematic(id, request);
            return Ok(response);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Delete a relation.
    /// </summary>
    /// <param name="id">The relation id.</param>
    /// <returns>IActionResult</returns>
    [HttpDelete("relations/{id}")]
    public async Task<IActionResult> Delete([FromRoute] Guid id)
    {
        try
        {
            var success = await relationService.Delete(id);
            return success ? Ok() : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
