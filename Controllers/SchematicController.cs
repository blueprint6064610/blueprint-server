using Blueprint.Models;
using Blueprint.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blueprint.Controllers;

/// <summary>
/// Class SchematicController.
/// </summary>
[ApiController]
[Authorize]
[Route("schematics")]
public class SchematicController : ControllerBase
{
    /// <summary>
    /// The schematic service.
    /// </summary>
    private readonly ISchematicService schematicService;

    /// <summary>
    /// The logger.
    /// </summary>
    private readonly ILogger<SchematicController> logger;

    /// <summary>
    /// SchematicController constructor.
    /// </summary>
    public SchematicController(ISchematicService schematicService, ILogger<SchematicController> logger)
    {
        this.schematicService = schematicService;
        this.logger = logger;
    }

    /// <summary>
    /// Create a schematic.
    /// </summary>
    /// <param name="request">The create schematic request.</param>
    /// <returns>The created schematic.</returns>
    [HttpPost]
    public async Task<IActionResult> Create([FromBody] SchematicCreateRequest request)
    {
        try
        {
            var success = await schematicService.Create(request);
            return success ? Ok() : BadRequest();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Delete a schematic.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>Success if deleted.</returns>
    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete([FromRoute] Guid id)
    {
        try
        {
            var success = await schematicService.Delete(id);
            return success ? Ok() : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Get schematics that match the query.
    /// </summary>
    /// <param name="request">The query request.</param>
    /// <returns>The schematics that matched.</returns>
    [HttpGet]
    public async Task<IActionResult> Find([FromQuery] SchematicFindRequest request)
    {
        try
        {
            var schematics = await schematicService.Find(request);
            return Ok(schematics);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Get a schematic by id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns>The schematic if found.</returns>
    [HttpGet("{id}")]
    public async Task<IActionResult> FindById([FromRoute] Guid id)
    {
        try
        {
            var schematic = await schematicService.FindById(id);
            return schematic != null ? Ok(schematic) : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Update a schematic.
    /// </summary>
    /// <param name="id">The schematic id.</param>
    /// <param name="request">The update request.</param>
    /// <returns>If successful.</returns>
    [HttpPut("{id}")]
    public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] SchematicUpdateRequest request)
    {
        try
        {
            var success = await schematicService.Update(id, request);
            return success ? Ok() : BadRequest();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
