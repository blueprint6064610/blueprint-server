using Blueprint.Services;
using Blueprint.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Blueprint.Controllers;

/// <summary>
/// Class CustomController.
/// </summary>
[ApiController]
[Authorize]
[Route("custom")]
public class CustomController : ControllerBase
{
    /// <summary>
    /// The custom service.
    /// </summary>
    private readonly ICustomService customService;

    /// <summary>
    /// The logger.
    /// </summary>
    private readonly ILogger<CustomController> logger;

    /// <summary>
    /// CustomController constructor.
    /// </summary>
    public CustomController(ICustomService customService, ILogger<CustomController> logger)
    {
        this.customService = customService;
        this.logger = logger;
    }

    /// <summary>
    /// Create a custom entity.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="request">The create entity request.</param>
    /// <returns>If successful.</returns>
    [HttpPost("{schematicId}")]
    public async Task<IActionResult> Create([FromRoute] Guid schematicId, [FromBody] JsonElement request)
    {
        try
        {
            var success = await customService.Create(schematicId, request);
            return success ? Ok() : BadRequest();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Delete a custom entity.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="id">The entity id.</param>
    /// <returns>If successful.</returns>
    [HttpDelete("{schematicId}/{id}")]
    public async Task<IActionResult> Delete([FromRoute] Guid schematicId, [FromRoute] Guid id)
    {
        try
        {
            var success = await customService.Delete(schematicId, id);
            return success ? Ok() : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Find custom entities matching the query.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="request">The query request.</param>
    /// <returns>The query response.</returns>
    [HttpGet("{schematicId}")]
    public async Task<IActionResult> Find([FromRoute] Guid schematicId)
    {
        try
        {
            var query = this.HttpContext.Request.Query.ToJsonElement();
            var entities = await customService.Find(schematicId, query);
            return Ok(entities);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Find a custom entity by id.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="entityId">The custom entity id.</param>
    /// <returns>The entity if found.</returns>
    [HttpGet("{schematicId}/{entityId}")]
    public async Task<IActionResult> FindById([FromRoute] Guid schematicId, [FromRoute] Guid entityId)
    {
        try
        {
            var entity = await customService.FindById(schematicId, entityId);
            return entity != null ? Ok(entity) : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Update a custom entity.
    /// </summary>
    /// <param name="schematicId">The schematic id.</param>
    /// <param name="entityId">The custom entity id.</param>
    /// <param name="request">The update request.</param>
    /// <returns>If sucessful.</returns>
    [HttpPut("{schematicId}/{entityId}")]
    public async Task<IActionResult> Update([FromRoute] Guid schematicId, [FromRoute] Guid entityId, [FromBody] JsonElement request)
    {
        try
        {
            var success = await customService.Update(schematicId, entityId, request);
            return success ? Ok() : BadRequest();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
