using Blueprint.Models;
using Blueprint.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blueprint.Controllers;

/// <summary>
/// Class UserController.
/// </summary>
[ApiController]
[Authorize]
[Route("users")]
public class UserController : ControllerBase
{
    /// <summary>
    /// The user service.
    /// </summary>
    private readonly IUserService userService;

    /// <summary>
    /// The logger.
    /// </summary>
    private readonly ILogger<UserController> logger;

    /// <summary>
    /// UserController constructor.
    /// </summary>
    public UserController(IUserService userService, ILogger<UserController> logger)
    {
        this.userService = userService;
        this.logger = logger;
    }

    /// <summary>
    /// Creates a new user.
    /// </summary>
    /// <param name="request">The create user request.</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> Create([FromBody] UserCreateRequest request)
    {
        try
        {
            var success = await userService.Create(request);
            return success ? Ok() : BadRequest();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Deletes an existing user.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete([FromRoute] Guid id)
    {
        try
        {
            var success = await userService.Delete(id);
            return success ? Ok() : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Gets all users.
    /// </summary>
    /// <returns>The list of users.</returns>
    [HttpGet]
    public async Task<IActionResult> Find([FromQuery] UserFindRequest request)
    {
        try
        {
            var users = await userService.Find(request);
            return Ok(users);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Gets the user with the unique id.
    /// </summary>
    /// <param name="id">The id.</param>
    /// <returns></returns>
    [HttpGet("{id}")]
    public async Task<IActionResult> FindById([FromRoute] Guid id)
    {
        try
        {
            var user = await userService.FindById(id);
            return user != null ? Ok(user) : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }

    /// <summary>
    /// Updates an existing user.
    /// </summary>
    /// <param name="id">The id of the user.</param>
    /// <param name="request">The update user request.</param>
    /// <returns></returns>
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateUser([FromRoute] Guid id, [FromBody] UserUpdateRequest request)
    {
        try
        {
            var success = await userService.Update(id, request);
            return success ? Ok() : NotFound();
        }
        catch (Exception e)
        {
            logger.LogError(e, "Internal server error: ");
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
